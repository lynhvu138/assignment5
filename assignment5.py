#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify, Response
import random
import json

app = Flask(__name__)

# Dictionary of books
books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]



@app.route('/book/JSON/')
def bookJSON():
    r = json.dumps(books)
    return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'POST':
        new_name = request.form['name']
        last_id = books[len(books) - 1]['id']
        new_id = int(last_id) + 1
        books.append({'title' : new_name, 'id': new_id})
        return redirect(url_for('showBook'))
    else:
        return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'POST':
        new_name = request.form['name']
        for i in range (len(books)):
            if int(books[i]['id']) == int(book_id):
                books[i]['title'] = new_name #change the name
                return redirect(url_for('showBook'))
    else:
        return render_template('editBook.html')
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == 'POST':
        for i in range (len(books)):
            if int(books[i]['id']) == int(book_id):
                del books[i]
                for j in range (i, len(books)):
                    new_id = int(books[j]['id']) - 1
                    books[j]['id'] = new_id
                return redirect(url_for('showBook'))
    else:
        for i in range (len(books)):
            if int(books[i]['id']) == int(book_id):
                return render_template('deleteBook.html', title=books[i]['title'])

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)